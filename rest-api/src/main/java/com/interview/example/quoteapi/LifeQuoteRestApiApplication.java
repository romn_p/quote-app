package com.interview.example.quoteapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LifeQuoteRestApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(LifeQuoteRestApiApplication.class, args);
	}

}
