package com.interview.example.quoteapi.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.apache.commons.lang3.Validate;

import com.fasterxml.jackson.annotation.JsonFormat;

import io.swagger.annotations.ApiModelProperty;

@Entity
public class Quote implements Serializable {

	private static final long serialVersionUID = -1085726093293265984L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	@Enumerated(EnumType.STRING)
	private Gender gender;
	@ApiModelProperty(example = "1976-11-01")
	@JsonFormat(pattern = "yyyy-MM-dd")
	private LocalDate dateOfBirth;
	@Enumerated(EnumType.STRING)
	private HealthCondition healthCondition;
	private String state;
	private Boolean smoker;
	private BigDecimal coverageAmount;
	private Integer term;
	private BigDecimal premiumAmount;

	public static final BigDecimal MIN_COVERAGE_AMOUNT = BigDecimal.valueOf(0);
	public static final BigDecimal MAX_COVERAGE_AMOUNT = BigDecimal.valueOf(5000000L);

	public static final Integer MIN_TERM = Integer.valueOf(10);
	public static final Integer MAX_TERM = Integer.valueOf(30);

	/**
	 * Used only by JPA
	 */
	protected Quote() {
		this.premiumAmount = BigDecimal.ZERO;
		this.coverageAmount = BigDecimal.ZERO;
	}

	/**
	 * Calculates the quote price
	 * 
	 * To make it simple the premium is 0.1% of the coverage amount for FEMALEs and
	 * 0.3% for MALEs
	 * 
	 */
	public void calculatePremium() {

		double percentage = 0;
		if (Gender.FEMALE.equals(this.gender))
			percentage = 0.1;
		if (Gender.MALE.equals(this.gender))
			percentage = 0.3;

		this.premiumAmount = coverageAmount.multiply(BigDecimal.valueOf(percentage).divide(BigDecimal.valueOf(100)));
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Gender getGender() {
		return gender;
	}

	public void setGender(Gender gender) {
		this.gender = gender;
	}

	public LocalDate getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(LocalDate dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public HealthCondition getHealthCondition() {
		return healthCondition;
	}

	public void setHealthCondition(HealthCondition healthCondition) {
		this.healthCondition = healthCondition;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public Boolean getSmoker() {
		return smoker;
	}

	public void setSmoker(Boolean smoker) {
		this.smoker = smoker;
	}

	public BigDecimal getCoverageAmount() {
		return coverageAmount;
	}

	public void setCoverageAmount(BigDecimal coverageAmount) {
		Validate.exclusiveBetween(MIN_COVERAGE_AMOUNT, MAX_COVERAGE_AMOUNT, coverageAmount,
				"Coverage amount not allowed");

		this.coverageAmount = coverageAmount;
	}

	public Integer getTerm() {
		return term;
	}

	public void setTerm(Integer term) {
		Validate.exclusiveBetween(MIN_TERM, MAX_TERM, term, "Policy term not allowed");
		this.term = term;
	}

	public BigDecimal getPremiumAmount() {
		return premiumAmount;
	}

	public void setPremiumAmount(BigDecimal premiumAmount) {
		this.premiumAmount = premiumAmount;
	}

}
