package com.interview.example.quoteapi.infrastructure.rest;

import java.io.IOException;

import javax.servlet.http.HttpServletResponse;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.interview.example.quoteapi.domain.QuoteNotFoundException;

/**
 * @author Roman
 * 
 *         Advice for exception handling and produce the corresponding HTTP
 *         status
 */
@ControllerAdvice
public class ExceptionAdvice extends ResponseEntityExceptionHandler {

	@ResponseBody
	@ExceptionHandler(QuoteNotFoundException.class)
	public void quoteNotFoundHandler(HttpServletResponse response) throws IOException {
		// keeping Spring default basic error message, just changing the status code to 404
		response.sendError(HttpStatus.NOT_FOUND.value());
	}
}
