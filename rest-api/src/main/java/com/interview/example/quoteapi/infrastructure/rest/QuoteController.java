package com.interview.example.quoteapi.infrastructure.rest;

import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.interview.example.quoteapi.domain.Quote;
import com.interview.example.quoteapi.domain.QuoteNotFoundException;
import com.interview.example.quoteapi.infrastructure.database.QuoteRepository;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@Api(tags = {"Quote"})
public class QuoteController {

	private QuoteRepository repository;

	/**
	 * Auto-wiring by constructor
	 * 
	 * @param store
	 */
	public QuoteController(QuoteRepository repository) {
		this.repository = repository;
	}

	@GetMapping(value = "/quotes/{quoteId}", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Retrieves a quote entity", tags = {"Quote"})
	public ResponseEntity<Quote> retrieveQuote(@PathVariable(name = "quoteId") Long quoteId) {
		Quote quote = repository.findById(quoteId).orElseThrow(() -> new QuoteNotFoundException(quoteId));
		return new ResponseEntity<>(quote, HttpStatus.OK);
	}

	@GetMapping(value = "/quotes", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Retrieves all quotes from the repository", tags = {"Quote"})
	public ResponseEntity<List<Quote>> retrieveQuotes() {
		List<Quote> quotes = repository.findAll();
		return new ResponseEntity<>(quotes, HttpStatus.OK);
	}

	@PostMapping(value = "/quotes", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Saves a new quote in the repository", tags = {"Quote"})
	public ResponseEntity<Quote> saveQuote(@RequestBody Quote quote) {
		Quote savedQuote = repository.save(quote);
		return new ResponseEntity<>(savedQuote, HttpStatus.CREATED);
	}

	@PutMapping(value = "/quotes/{quoteId}", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Updates and existing quote in the repository", tags = {"Quote"})
	public ResponseEntity<Quote> updateQuote(@PathVariable Long quoteId, @RequestBody Quote quote) {

		// checking if the quote exists for the given id
		if (!repository.existsById(quoteId))
			throw new QuoteNotFoundException(quoteId);

		// updating the given quote
		Quote updatedQuote = repository.save(quote);
		return new ResponseEntity<>(updatedQuote, HttpStatus.OK);
	}

	@DeleteMapping(value = "/quotes/{quoteId}", consumes = MediaType.APPLICATION_JSON_VALUE)
	@ResponseStatus(value = HttpStatus.OK)
	public void deleteQuote(@PathVariable(name = "quoteId") Long quoteId) {
		repository.deleteById(quoteId);
	}
	
	@PostMapping(value = "/quotes/prices", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Calculates the premium for a given quote", tags = {"Quote"})
	public ResponseEntity<Quote> calculatePremium(@RequestBody Quote quote) {
		quote.calculatePremium();
		return new ResponseEntity<>(quote, HttpStatus.OK);
	}	

}
