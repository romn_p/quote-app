package com.interview.example.quoteapi.domain;

public class QuoteNotFoundException extends RuntimeException {

	private static final long serialVersionUID = -2418490289349659954L;

	public QuoteNotFoundException(Long id) {
		super("Quote not foudnd quoteId: " + id);
	}

}
