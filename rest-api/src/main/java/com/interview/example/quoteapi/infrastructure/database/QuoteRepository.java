package com.interview.example.quoteapi.infrastructure.database;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.interview.example.quoteapi.domain.Quote;

public interface QuoteRepository extends CrudRepository<Quote, Long> {
	
	@Override
	List<Quote> findAll();

}
