package com.interview.example.quoteapi.config;

import java.util.Collections;

import org.apache.commons.lang3.StringUtils;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.bind.annotation.RestController;

import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.service.Tag;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class SwaggerConfig {

	@Bean
	public Docket quoteApi() {
		return new Docket(DocumentationType.SWAGGER_2).select()
				.apis(RequestHandlerSelectors.withClassAnnotation(RestController.class)).paths(PathSelectors.any())
				.build().apiInfo(apiInfo()).tags(new Tag("Quote", "All operations relating to quote"));
	}

	private ApiInfo apiInfo() {
		return new ApiInfo("Demo - Life Insurance Quote API", "Demo application for developer position at Massmutual. A REST API exposing operations relating to LifeInsurance quotes. The API will support a front-end application built with React.",
				"1.0", StringUtils.EMPTY, contactInfo(), StringUtils.EMPTY, StringUtils.EMPTY, Collections.emptyList());
	}

	private Contact contactInfo() {
		return new Contact("Roman Pena", "https://bitbucket.org/romn_p/quote-app", "romanpf@gmail.com");
	}

}
