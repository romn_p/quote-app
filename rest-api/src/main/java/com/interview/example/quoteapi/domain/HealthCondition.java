package com.interview.example.quoteapi.domain;

public enum HealthCondition {
	
	EXCELLENT, GOOD, AVERAGE;

}
