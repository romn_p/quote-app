package com.interview.example.quoteapi.domain;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.math.BigDecimal;
import java.time.LocalDate;

import org.junit.Test;

public class QuoteTest {

	@Test(expected = IllegalArgumentException.class)
	public void givenAnInvalidPolicyTermAnErrorShouldBeThrown() {

		Integer invalidTerm = Integer.valueOf(100);
		Quote aQuote = new Quote();

		aQuote.setGender(Gender.FEMALE);
		aQuote.setState("MA");
		aQuote.setHealthCondition(HealthCondition.EXCELLENT);
		aQuote.setDateOfBirth(LocalDate.of(1980, 1, 1));
		aQuote.setCoverageAmount(BigDecimal.valueOf(1000000));
		aQuote.setTerm(invalidTerm);
	}

	@Test
	public void givenAValidPolicyTermTheQuoteInstanceShouldBeCreated() {

		Integer term = Integer.valueOf(20);
		BigDecimal coverageAmount = BigDecimal.valueOf(1000000);

		Quote aQuote = new Quote();
		aQuote.setGender(Gender.FEMALE);
		aQuote.setState("MA");
		aQuote.setDateOfBirth(LocalDate.of(1980, 1, 1));
		aQuote.setCoverageAmount(BigDecimal.valueOf(1000000));
		aQuote.setTerm(term);

		assertNotNull(aQuote);
		assertEquals(aQuote.getCoverageAmount(), coverageAmount);
		assertEquals(aQuote.getTerm(), term);

	}

	@Test
	public void givenAQuoteWhenGenderIsFemaleThenPriceIsOnePercent() {
		BigDecimal theCoverageAmount = BigDecimal.valueOf(1000000);
		Quote aQuote = new Quote();
		aQuote.setGender(Gender.FEMALE);
		aQuote.setState("MA");
		aQuote.setDateOfBirth(LocalDate.of(1980, 1, 1));
		aQuote.setCoverageAmount(theCoverageAmount);
		aQuote.setTerm(Integer.valueOf(20));
		// calculating the price
		aQuote.calculatePremium();
		assertEquals(1000, aQuote.getPremiumAmount().intValue());

	}
	
	@Test
	public void givenAQuoteWhenGenderIsMaleThenPriceIsThreePercent() {
		BigDecimal theCoverageAmount = BigDecimal.valueOf(1000000);
		Quote aQuote = new Quote();
		aQuote.setGender(Gender.MALE);
		aQuote.setState("MA");
		aQuote.setDateOfBirth(LocalDate.of(1980, 1, 1));
		aQuote.setCoverageAmount(theCoverageAmount);
		aQuote.setTerm(Integer.valueOf(20));
		// calculating the price
		aQuote.calculatePremium();
		assertEquals(3000, aQuote.getPremiumAmount().intValue());

	}	

}
