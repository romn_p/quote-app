import React, { Component } from "react";
import QuoteService from "./services/quoteservice";
import QuoteForm from "./components/quoteform";
import QuoteList from "./components/quotelist";
import QuoteCount from "./components/quotecount";

class App extends Component {
  constructor(props) {
    super(props);
    this.quoteService = new QuoteService();
  }

  state = {
    quotes: []
  };

  async componentDidMount() {
    this.retrieveQuotes();
  }

  /* 
  Handles retrieval of all quotes stored in the database
  */
  retrieveQuotes() {
    this.quoteService.retrieveQuotes().then(quotes => {
      this.setState({ quotes: quotes });
    });
  }

  /* 
  Handles a quote submission
  */
  handleSubmitQuote = quote => {
    this.quoteService.saveQuote(quote).then(quote => {
      this.retrieveQuotes();
    });
  };

  /* 
  Handles a quote deletion
  */
  handleDeleteQuote = quoteId => {
    this.quoteService.deleteQuote(quoteId).then(() => {
      this.retrieveQuotes();
    });
  };

  render() {
    return (
      <React.Fragment>
        <div className="row">
          <div className="col-sm-4">
            <div>
              <h1>Enter quote data</h1>
            </div>
            <QuoteForm onSubmitQuote={this.handleSubmitQuote} />
          </div>
          <div className="col-sm-8">
            <div>
              <h1>
                You have <QuoteCount totalQuotes={this.state.quotes.length} /> saved quotes
              </h1>
            </div>
            <QuoteList quotes={this.state.quotes} onDeleteQuote={this.handleDeleteQuote} />
          </div>
        </div>
      </React.Fragment>
    );
  }
}

export default App;
