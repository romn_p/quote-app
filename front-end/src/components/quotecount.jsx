import React, { Component } from "react";
/*
A simple control to display the number of saved quotes
*/
class QuoteCount extends Component {
  render() {
    return (
      <React.Fragment>
        <span className="text-info">{this.props.totalQuotes}</span>
      </React.Fragment>
    );
  }
}

export default QuoteCount;
