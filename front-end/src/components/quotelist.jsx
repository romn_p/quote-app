import React, { Component } from "react";
import Quote from "./quote";

/*
The Quote component displays a list of quotes, it expects an array of quotes objects in props.quotes
*/
class QuoteList extends Component {
  render() {
    return (
      <React.Fragment>
        <table className="table">
          <thead>
            <tr>
              <th scope="col">#</th>
              <th scope="col">Gender</th>
              <th scope="col">D.O.B.</th>
              <th scope="col">Health</th>
              <th scope="col">Smoker</th>
              <th scope="col">Coverage($)</th>
              <th scope="col">Term(yrs)</th>
              <th scope="col">Price($)</th>
              <th scope="col" />
            </tr>
          </thead>
          <tbody>
            {this.props.quotes.map(q => (
              <Quote key={q.id} quote={q} onDeleteQuote={this.props.onDeleteQuote} />
            ))}
          </tbody>
        </table>
      </React.Fragment>
    );
  }
}

export default QuoteList;
