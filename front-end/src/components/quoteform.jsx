import React, { Component } from "react";
import QuoteService from "../services/quoteservice";

/*
Renders a form for enter the quote information, also it makes a REST call to calculate the price of the quote
*/
class QuoteForm extends Component {
  constructor(props) {
    super(props);
    this.quoteService = new QuoteService();
    this.defaultQuoteData = {
      gender: "FEMALE",
      dateOfBirth: "1980-01-01",
      healthCondition: "EXCELLENT",
      state: "MA",
      smoker: "false",
      coverageAmount: "100000",
      term: "30",
      premiumAmount: "0"
    };
    this.state = this.defaultQuoteData;
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleCalculateQuote = this.handleCalculateQuote.bind(this);
    this.handleReset = this.handleReset.bind(this);
  }

  handleChange(event) {
    const target = event.target;
    const value = target.value;
    const name = target.name;
    this.setState({ [name]: value });
  }

  handleSubmit(event) {
    this.props.onSubmitQuote(this.state);
    // reseting to defaults values
    this.setState(this.defaultQuoteData);
    event.preventDefault();
  }

  handleCalculateQuote(event) {
    this.quoteService.calculateQuote(this.state).then(quote => {
      //updating the premium amount with the calculated price
      this.setState({ premiumAmount: quote.premiumAmount });
    });
    event.preventDefault();
  }

  disableSaveButton() {
    return this.state.premiumAmount === "0";
  }

  handleReset(event) {
    this.setState(this.defaultQuoteData);
    event.preventDefault();
  }

  render() {
    return (
      <React.Fragment>
        <form onSubmit={this.handleSubmit}>
          <div className="form-group">
            <label>Gender</label>
            <select name="gender" value={this.state.gender} className="custom-select" onChange={this.handleChange}>
              <option value="FEMALE">Female</option>
              <option value="MALE">Male</option>
            </select>
          </div>
          <div className="form-group">
            <label>State of Residence</label>
            <select name="state" value={this.state.state} className="custom-select" onChange={this.handleChange}>
              <option>ALABAMA</option>
              <option>ALASKA</option>
              <option>ARIZONA</option>
              <option>ARKANSAS</option>
              <option>CALIFORNIA</option>
              <option>COLORADO</option>
              <option>CONNECTICUT</option>
              <option>DELAWARE</option>
              <option>DISTRICT OF COLUMBIA</option>
              <option>FLORIDA</option>
              <option>GEORGIA</option>
              <option>HAWAII</option>
              <option>IDAHO</option>
              <option>ILLINOIS</option>
              <option>INDIANA</option>
              <option>IOWA</option>
              <option>KANSAS</option>
              <option>KENTUCKY</option>
              <option>LOUISIANA</option>
              <option>MAINE</option>
              <option>MARYLAND</option>
              <option>MASSACHUSETTS</option>
              <option>MICHIGAN</option>
              <option>MINNESOTA</option>
              <option>MISSISSIPPI</option>
              <option>MISSOURI</option>
              <option>MONTANA</option>
              <option>NEBRASKA</option>
              <option>NEVADA</option>
              <option>NEW HAMPSHIRE</option>
              <option>NEW JERSEY</option>
              <option>NEW MEXICO</option>
              <option>NEW YORK</option>
              <option>NORTH CAROLINA</option>
              <option>NORTH DAKOTA</option>
              <option>OHIO</option>
              <option>OKLAHOMA</option>
              <option>OREGON</option>
              <option>PALAU</option>
              <option>PENNSYLVANIA</option>
              <option>RHODE ISLAND</option>
              <option>SOUTH CAROLINA</option>
              <option>SOUTH DAKOTA</option>
              <option>TENNESSEE</option>
              <option>TEXAS</option>
              <option>UTAH</option>
              <option>VERMONT</option>
              <option>VIRGINIA</option>
              <option>WASHINGTON</option>
              <option>WEST VIRGINIA</option>
              <option>WISCONSIN</option>
              <option>WYOMING</option>
            </select>{" "}
          </div>
          <div className="form-group">
            <label>Date of birth</label>
            <input
              type="text"
              name="dateOfBirth"
              value={this.state.dateOfBirth}
              className="form-control"
              onChange={this.handleChange}
            />
          </div>
          <div className="form-group">
            <label>Health condition</label>
            <select
              name="healthCondition"
              value={this.state.healthCondition}
              className="custom-select"
              onChange={this.handleChange}
            >
              <option>EXCELLENT</option>
              <option>GOOD</option>
              <option>AVERAGE</option>
            </select>
          </div>
          <div className="form-group">
            <label>Smoker</label>
            <select name="smoker" value={this.state.smoker} className="custom-select" onChange={this.handleChange}>
              <option value="true">YES</option>
              <option value="false">NO</option>
            </select>
          </div>
          <div className="form-group">
            <label>Coverage Amount</label>
            <input
              type="number"
              name="coverageAmount"
              value={this.state.coverageAmount}
              className="form-control"
              onChange={this.handleChange}
            />
          </div>
          <div className="form-group">
            <label>Term</label>
            <select name="term" value={this.state.term} className="custom-select" onChange={this.handleChange}>
              <option>10</option>
              <option>15</option>
              <option>20</option>
              <option>25</option>
              <option>30</option>
            </select>
          </div>
          <div className="form-group">
            <label>Premium Amount</label>
            <input
              type="number"
              name="premiumAmount"
              value={this.state.premiumAmount}
              className="form-control"
              readOnly={true}
            />
          </div>
          <div className="btn-toolbar">
            <div className="btn-group mr-2">
              <button type="button" className="btn btn-primary" onClick={this.handleCalculateQuote}>
                Calculate Price ($)
              </button>
            </div>
            <div className="btn-group mr-2">
              <button
                type="submit"
                className="btn btn-primary"
                disabled={this.disableSaveButton()}
                title="Please calculate a price before saving"
              >
                Save
              </button>
            </div>
            <div className="btn-group mr-2">
              <button type="reset" className="btn btn-primary" onClick={this.handleReset}>
                Reset
              </button>
            </div>
          </div>
        </form>
      </React.Fragment>
    );
  }
}

export default QuoteForm;
