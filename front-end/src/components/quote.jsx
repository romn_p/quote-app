import React, { Component } from "react";

/*
The component displays the data of a single quote, it expects a quote instance as a property
*/
class Quote extends Component {
  render() {
    return (
      <React.Fragment>
        <tr>
          <th scope="row">{this.props.quote.id}</th>
          <td>{this.props.quote.gender}</td>
          <td>{this.props.quote.dateOfBirth}</td>
          <td>{this.props.quote.healthCondition}</td>
          <td>{this.props.quote.smoker ? "Yes" : "No"}</td>
          <td>{this.props.quote.coverageAmount}</td>
          <td>{this.props.quote.term}</td>
          <td>{this.props.quote.premiumAmount}</td>
          <td>
            <button
              type="button"
              className="btn btn-outline-danger btn-sm float-right"
              onClick={() => this.props.onDeleteQuote(this.props.quote.id)}
            >
              Delete
            </button>
          </td>
        </tr>
      </React.Fragment>
    );
  }
}

export default Quote;
