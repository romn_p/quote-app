/*
This class encapsulates all the REST calls
*/
class QuoteService {
  constructor() {
    this.quotes = [];
  }

  async retrieveQuotes() {
    // calling REST API operation to retrieve all quotes GET -> /quotes
    const httpResponse = await fetch("quotes");
    // checking HTTP error
    if (!httpResponse.ok) this.handleHTTPError(httpResponse);
    // Success response, get JSON body
    const response = await httpResponse.json();
    return response;
  }

  async retrieveQuote(quoteId) {
    // calling REST API operation to retrieve a quotes by Id GET -> /quotes/id
    const httpResponse = await fetch(`quotes/${quoteId}`);
    // checking HTTP error
    if (!httpResponse.ok) this.handleHTTPError(httpResponse);
    // Success response, get JSON body
    const response = await httpResponse.json();
    return response;
  }

  async saveQuote(quote) {
    // calling REST API operation to save a quote POST -> /quotes
    const httpResponse = await fetch("quotes", {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify(quote)
    });
    // checking HTTP error
    if (!httpResponse.ok) this.handleHTTPError(httpResponse);
    // Success response, get JSON body
    const response = await httpResponse.json();
    return response;
  }

  async deleteQuote(quoteId) {
    // calling REST API operation to delete a quote DELETE -> /quotes/quoteid
    const httpResponse = await fetch(`quotes/${quoteId}`, {
      method: "DELETE",
      headers: { "Content-Type": "application/json" }
    });
    // checking HTTP error
    if (!httpResponse.ok) this.handleHTTPError(httpResponse);
  }

  async calculateQuote(quote) {
    // calling REST API operation to calculate the price of a quote POST -> /quotes/prices
    const httpResponse = await fetch("quotes/prices", {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify(quote)
    });
    // checking HTTP error
    if (!httpResponse.ok) this.handleHTTPError(httpResponse);
    // Success response, get JSON body
    const response = await httpResponse.json();
    return response;
  }

  handleHTTPError(response) {
    throw new Error("HTTP error, status = " + response.status);
  }
}

export default QuoteService;
