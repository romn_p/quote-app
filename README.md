## Life Insurance Quote Application

### Demo application for developer position at MassMutual.

### Candidate: Roman Pena

### Introduction

As part of the interview process I am building an end-to-end DEMO application (fom UI to Database). The sample application allows to manage life insurance quotes. The users will be able to create, calculate and save quotes into the database. The application is composed by a front-end user interface integrated with a Web REST API, the API exposes operations to access the database, and an extra operation to calculate the price of a quote.

### Technologies

**Front-end:** React, NodeJS, Bootstrap

**Middleware:** Java, Spring Boot, Spring Data JPA, Swagger, JUnit

**Database:** H2 (embeded in memory database)

### Requirements

- JDK 1.8
- Maven 3.6.1
- NodeJS v10.15.3

### Build and Installation

Clone the source code repository -> git clone https://romn_p@bitbucket.org/romn_p/quote-app.git

The repository contains both the back-end REST API, and the fron-end application.

### Installing the REST API

The REST API is a regular Java Maven project. Go to the folder /quote-api (where there pom file is located) and execute "mvn clean install".

(Optional) Edit the file /src/main/resources/application.yml to run the app in a different port (default 8080).

An executable jar should be generated inside the "target" folder (quote-api-1.0.0.jar).

The JAR can be execute using: java -jar target/quote-api-1.0.0.jar

Once the application is up and running please access the Swagger API documentation at: http://localhost:8080/quote-api/swagger-ui.html

The service handles requests for create, retrieves, delete, and calculate quotes.

You can use the Swagger-UI, any REST client tool or just CURL to test the API. start creating few quotes (POST) and to prove they are saved into the database access the H2 console as indicated below.

### Database (H2 in-memory)

For simplicy an embedded H2 database is used. Once the application is running the database console is available at http://localhost:8080/quote-app/h2-console (just click Connect).

### Front-end application (UI) - React

Basic application built in React. The tool "Create React App" was used for the development.

The front-end project is a regular Node package. Go to the folder /front-end (where there package.json file is located) and execute "npm install".

(Optional) Edit the file package.json to set a different proxy in case the back-end API is running in a different port, default (http://localhost:8080/quote-api/).

Start the front-end application by executing the command "npm start", the application should start at http://localhost:3000/

Using the demo app you should be able to rate life insurance quotes and save them into the database.

Please let me know if you have any question.

Thank you,
Roman

### The front-end application should look like this

![](front-end-screen.png)
